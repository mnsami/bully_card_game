<?php
declare(strict_types=1);

use BullyCardGame\Application\Service\Exception\SorryPlayerHasNoMatchingCard;
use BullyCardGame\Application\Service\GameSetupService;
use BullyCardGame\Application\Service\DealPlayersService;
use BullyCardGame\Application\Service\FlipTopCardFromDeckService;
use BullyCardGame\Application\Service\PlayerPlayTurnService;
use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Player\Player;
use BullyCardGame\Infrastructure\Repository\Card\InMemoryCardRepository;
use BullyCardGame\Infrastructure\Repository\Player\InMemoryPlayerRepository;
use BullyCardGame\Infrastructure\UI\Console\ConsoleOutput;
use BullyCardGame\Infrastructure\UI\Output;
use BullyCardGame\Domain\Model\Card\CardRepository;
use BullyCardGame\Domain\Model\Player\PlayerRepository;

final class Game
{
    private const MIN_PLAYERS = 2;

    /** @var GameSetupService */
    private $gameSetupService;

    /** @var DealPlayersService */
    private $dealPlayersService;

    /** @var PlayerPlayTurnService */
    private $playerPlayTurnService;

    /** @var FlipTopCardFromDeckService */
    private $flipTopCardFromDeckService;

    /** @var PlayerRepository */
    private $playerRepository;

    /** @var CardRepository */
    private $cardRepository;

    /** @var Output */
    private $output;

    /** @var string[] */
    private $playerNames;

    private function __construct(array $playerNames)
    {
        $this->playerRepository = new InMemoryPlayerRepository();
        $this->cardRepository = new InMemoryCardRepository();

        $this->gameSetupService = new GameSetupService(
            $this->cardRepository,
            $this->playerRepository
        );

        $this->dealPlayersService = new DealPlayersService(
            $this->cardRepository,
            $this->playerRepository
        );

        $this->playerPlayTurnService = new PlayerPlayTurnService(
            $this->playerRepository,
            $this->cardRepository
        );

        $this->flipTopCardFromDeckService = new FlipTopCardFromDeckService(
            $this->cardRepository
        );

        $this->output = new ConsoleOutput();

        $this->playerNames = $playerNames;
    }

    public static function createGameWithPlayers(array $playerNames): Game
    {
        if (count($playerNames) < self::MIN_PLAYERS) {
            throw new \InvalidArgumentException("Sorry, not enough players.");
        }

        return new Game($playerNames);
    }


    public function setup()
    {
        $this->gameSetupService->execute($this->playerNames);
    }

    public function run()
    {
        /** @var Player[] $players */
        $players = $this->playerRepository->players();

        $nextCard = $this->flipTopCardFromDeckService->execute();
        $this->output->info("Top card is: " . $nextCard->toString() . "\n");

        while (true) {
            foreach ($players as $player) {
                try {
                    $nextCard = $this->playerPlayTurnService->execute($player->playerId(), $nextCard);
                    $this->output->info($player->name() . " plays " . $nextCard->toString() . "\n");
                    if ($player->outOfCards()) {
                        $this->output->success($player->name() . " has won.\n");
                        break 2;
                    }
                } catch (SorryPlayerHasNoMatchingCard $e) {
                    // player skips if no matching cards found
                    $card = $this->flipTopCardFromDeckService->execute();
                    $player->take(
                        $card
                    );
                    $this->output->warning(
                        $player->name() . " does not have a suitable card, taking from deck {$card->toString()}\n"
                    );
                    continue;
                }
            }
        }
    }

    public function dealCardsToPlayers()
    {
        $this->dealPlayersService->execute();

        $players = $this->playerRepository->players();

        foreach ($players as $player) {
            $cards = $player->cards();
            $cards = array_map(function (Card $card) {
                return $card->toString();
            }, $cards);
            $this->output->info($player->name() . " has been dealt: " . implode(" ", $cards) . "\n");
        }
    }
}
