<?php
declare(strict_types=1);

require_once 'vendor/autoload.php';
require_once __DIR__ . '/Game.php';

$game = Game::createGameWithPlayers(
    ["mina", 'hans', 'nico', 'lucas']
);

$game->setup();
$game->dealCardsToPlayers();

$game->run();
