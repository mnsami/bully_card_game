COMPOSER ?= composer
DOCKER_COMPOSE = docker-compose
PROJECT = "Bully Card game."

ifeq ($(RUNNER), travis)
	CMD :=
else
	CMD := docker-compose exec php
endif

all: container-up clear composer lint-composer lint-php phpcs play

lint-composer:
	@echo "\n==> Validating composer.json and composer.lock:"
	$(CMD) $(COMPOSER) validate --strict

lint-php:
	@echo "\n==> Validating all php files:"
	@find src -type f -name \*.php | while read file; do php -l "$$file" || exit 1; done

composer:
	$(CMD) $(COMPOSER) install

clear:
	$(CMD) rm -rf vendor
	$(CMD) rm -rf bin/php*

phpcs:
	$(CMD) php bin/phpcs --standard=phpcs.xml -p

phpcbf:
	$(CMD) bin/phpcbf

container-stop:
	@echo "\n==> Stopping docker container"
	$(DOCKER_COMPOSE) stop

container-down:
	@echo "\n==> Removing docker container"
	$(DOCKER_COMPOSE) down

container-up:
	@echo "\n==> Docker container building and starting ..."
	$(DOCKER_COMPOSE) up --build -d

tear-down: container-stop container-down

play:
	$(CMD) php bin/App.php

.PHONY: lint-php lint-composer phpcs phpcbf composer clear play container-up container-down container-stop tear-down
