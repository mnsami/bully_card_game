Bully Card Game
===============

### Installation

1. Build container 
    
    `make container-up`

2. Install dependencies

    `make compsoer`
    
3. Start playing

    `make play`
    
Or, just run everything at once with one command:

    `make all`
    
above command will do the following:

1. Remove `vendor` folder.
2. Lint `composer.json`
3. Lint all php files.
4. Install project dependencies.
5. Run code sniffer.
6. Start the game.
    
### Helpers

- Run code sniffing, for this project I'm using PSR-2 standards.

    `make phpcs`
    
-  For fixing sniffing errors.

    `make phpcbf`
    
### Playing the game.

- `make play`

