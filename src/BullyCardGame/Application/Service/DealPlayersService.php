<?php
declare(strict_types=1);

namespace BullyCardGame\Application\Service;

use BullyCardGame\Domain\Model\Card\CardRepository;
use BullyCardGame\Domain\Model\Player\Player;
use BullyCardGame\Domain\Model\Player\PlayerRepository;

class DealPlayersService
{
    private const CARDS_PER_PLAYER = 7;

    /** @var CardRepository */
    private $deckRepository;

    /** @var PlayerRepository */
    private $playerRepository;

    public function __construct(CardRepository $deckRepository, PlayerRepository $playerRepository)
    {
        $this->deckRepository = $deckRepository;
        $this->playerRepository = $playerRepository;
    }

    public function execute()
    {
        /** @var Player[] $players */
        $players = $this->playerRepository->players();

        foreach ($players as $player) {
            $cards = [];
            for ($idx = 0; $idx < self::CARDS_PER_PLAYER; $idx++) {
                $card = $this->deckRepository->pop();
                $cards[] = $card;
            }

            $player->deal(
                $cards
            );
        }
    }
}
