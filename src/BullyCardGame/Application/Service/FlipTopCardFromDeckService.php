<?php
declare(strict_types=1);

namespace BullyCardGame\Application\Service;

use BullyCardGame\Application\Service\Exception\SorryDeckIsEmpty;
use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Card\CardRepository;

class FlipTopCardFromDeckService
{
    /** @var CardRepository */
    private $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    public function execute(): Card
    {
        if (!$this->cardRepository->isEmpty()) {
            return $this->cardRepository->pop();
        }

        throw new SorryDeckIsEmpty("Deck out of cards.");
    }
}
