<?php
declare(strict_types=1);

namespace BullyCardGame\Application\Service;

use BullyCardGame\Application\Service\Exception\SorryPlayerHasNoMatchingCard;
use BullyCardGame\Application\Service\Exception\SorryPlayerNotFound;
use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Card\CardRepository;
use BullyCardGame\Domain\Model\Player\PlayerId;
use BullyCardGame\Domain\Model\Player\PlayerRepository;

class PlayerPlayTurnService
{
    /** @var PlayerRepository */
    private $playerRepository;

    /** @var CardRepository */
    private $cardRepository;

    public function __construct(PlayerRepository $playerRepository, CardRepository $cardRepository)
    {
        $this->playerRepository = $playerRepository;
        $this->cardRepository = $cardRepository;
    }

    public function execute(PlayerId $playerId, Card $card): Card
    {
        $player = $this->playerRepository->ofId($playerId);

        if ($player === null) {
            throw new SorryPlayerNotFound("Player not found.");
        }

        $card = $this->playerRepository->playerCardWith($playerId, $card->suit()->color(), $card->rank());

        if ($card === null) {
            throw new SorryPlayerHasNoMatchingCard("Player has no matching card.");
        }

        $player->playCard($card);

        return $card;
    }
}
