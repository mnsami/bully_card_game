<?php
declare(strict_types=1);

namespace BullyCardGame\Application\Service\Exception;

class SorryPlayerHasNoMatchingCard extends \RuntimeException
{
}
