<?php
declare(strict_types=1);

namespace BullyCardGame\Application\Service;

use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Card\CardRepository;
use BullyCardGame\Domain\Model\Player\Player;
use BullyCardGame\Domain\Model\Player\PlayerRepository;
use BullyCardGame\Domain\Model\Card\Rank;
use BullyCardGame\Domain\Model\Card\Suit;

class GameSetupService
{
    private const DECK_SIZE = 52;

    /** @var CardRepository */
    private $cardRepository;

    /** @var PlayerRepository */
    private $playerRepository;

    public function __construct(CardRepository $cardRepository, PlayerRepository $playerRepository)
    {
        $this->cardRepository = $cardRepository;
        $this->playerRepository = $playerRepository;
    }

    /**
     * @param string[] $playerNames
     */
    public function execute(array $playerNames)
    {
        foreach ($playerNames as $playerName) {
            if (gettype($playerName) !== "string") {
                throw new \InvalidArgumentException("PlayerName must be string.");
            }

            $this->playerRepository->add(
                new Player($this->playerRepository->nextIdentity(), $playerName)
            );
        }

        $this->shuffleDeck();
    }

    private function shuffleDeck()
    {
        $suits = Suit::SUITS;
        $ranks = Rank::RANKS;

        shuffle($suits);
        shuffle($ranks);

        $pickedRanks = $pickedSuits = [];

        while (count($this->cardRepository->cards()) < self::DECK_SIZE) {
            while (in_array($rankKey = rand(0, count($ranks) - 1), $pickedRanks)) {
                $pickedRanks[] = $rankKey;
            }

            while (in_array($suitKey = rand(0, count($suits) - 1), $pickedSuits)) {
                $pickedSuits[] = $suitKey;
            }

            $card = new Card(
                Suit::fromString($suits[$suitKey]),
                new Rank($ranks[$rankKey])
            );
            $this->cardRepository->push($card);
        }
    }
}
