<?php
declare(strict_types=1);

namespace BullyCardGame\Infrastructure\Repository\Deck\Exception;

class SorryDeckIsFull extends \RuntimeException
{
}
