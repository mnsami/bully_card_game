<?php
declare(strict_types=1);

namespace BullyCardGame\Infrastructure\Repository\Card;

use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Card\CardRepository;
use BullyCardGame\Infrastructure\Repository\Deck\Exception\SorryDeckIsFull;

class InMemoryCardRepository implements CardRepository
{
    private const DECK_SIZE = 52;

    /** @var Card[] */
    private $cards = [];

    public function push(Card $card)
    {
        if (count($this->cards) < self::DECK_SIZE) {
            array_push($this->cards, $card);
        } else {
            throw new SorryDeckIsFull("Sorry, Deck is full.");
        }
    }

    public function pop(): ?Card
    {
        return array_pop($this->cards);
    }

    public function isEmpty(): bool
    {
        if (count($this->cards) == 0) {
            return true;
        }

        return false;
    }

    /**
     * @return Card[]
     */
    public function cards(): array
    {
        return $this->cards;
    }
}
