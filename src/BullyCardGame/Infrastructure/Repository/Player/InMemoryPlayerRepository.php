<?php
declare(strict_types=1);

namespace BullyCardGame\Infrastructure\Repository\Player;

use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Card\Rank;
use BullyCardGame\Domain\Model\Card\Suit;
use BullyCardGame\Domain\Model\Player\Player;
use BullyCardGame\Domain\Model\Player\PlayerId;
use BullyCardGame\Domain\Model\Player\PlayerRepository;

class InMemoryPlayerRepository implements PlayerRepository
{
    /** @var Player[] */
    private $players = [];

    public function add(Player $player)
    {
        $this->players[$player->id()] = $player;
    }

    public function ofId(PlayerId $playerId): ?Player
    {
        if (!isset($this->players[$playerId->id()])) {
            return null;
        }

        return $this->players[$playerId->id()];
    }

    public function nextIdentity(): PlayerId
    {
        return new PlayerId();
    }

    /**
     * @return Player[]
     */
    public function players(): array
    {
        return $this->players;
    }

    public function playerCardWith(PlayerId $playerId, string $color, Rank $rank): ?Card
    {
        $player = $this->ofId($playerId);
        /** @var Card[] $cards */
        $cards = $player->cards();
        foreach ($cards as $card) {
            if ($card->rank()->equals($rank)
                || $card->suit()->color() === $color
            ) {
                return $card;
            }
        }

        return null;
    }
}
