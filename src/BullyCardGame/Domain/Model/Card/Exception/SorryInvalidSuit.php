<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Card\Exception;

class SorryInvalidSuit extends \InvalidArgumentException
{
}
