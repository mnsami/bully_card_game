<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Card;

use BullyCardGame\Domain\Model\Card\Exception\SorryInvalidSuit;

final class Suit
{
    /** @var string */
    private $suit;

    /** @var string */
    private $symbol;

    /** @var string */
    private $color;

    private const COLOR_RED = 'red';
    private const COLOR_BLACK = 'black';

    private const SUIT_DIAMONDS_SYMB = '♦';
    private const SUIT_DIAMONDS = "diamonds";

    private const SUIT_CLUBS_SYMB = '♣';
    private const SUIT_CLUBS = "clubs";

    private const SUIT_HEARTS_SYMB = '♥';
    private const SUIT_HEARTS = "hearts";

    private const SUIT_SPADES_SYMB = '♠';
    private const SUIT_SPADES = "spades";

    private const VALID_COLORS = [
        self::COLOR_BLACK,
        self::COLOR_RED
    ];

    private const COLOR_SUIT_MAP = [
        self::SUIT_DIAMONDS => self::COLOR_RED,
        self::SUIT_CLUBS => self::COLOR_BLACK,
        self::SUIT_HEARTS => self::COLOR_RED,
        self::SUIT_SPADES  => self::COLOR_BLACK,
    ];

    private const SUIT_SYMBOL_MAP = [
        self::SUIT_DIAMONDS => self::SUIT_DIAMONDS_SYMB,
        self::SUIT_CLUBS => self::SUIT_CLUBS_SYMB,
        self::SUIT_HEARTS => self::SUIT_HEARTS_SYMB,
        self::SUIT_SPADES => self::SUIT_SPADES_SYMB
    ];

    public const SUITS = [
        self::SUIT_DIAMONDS,
        self::SUIT_CLUBS,
        self::SUIT_HEARTS,
        self::SUIT_SPADES
    ];

    private function __construct(string $suit, string $symbol)
    {
        $this->suit = $suit;
        $this->symbol = $symbol;
        $this->color = self::COLOR_SUIT_MAP[$this->suit];
    }

    public static function fromString(string $suit): Suit
    {
        if (!in_array($suit, self::SUITS)) {
            throw new SorryInvalidSuit("Invalid Suit provided.");
        }

        return new Suit($suit, self::SUIT_SYMBOL_MAP[$suit]);
    }

    public static function hearts(): Suit
    {
        return new Suit(self::SUIT_HEARTS, self::SUIT_HEARTS_SYMB);
    }

    public static function diamonds(): Suit
    {
        return new Suit(self::SUIT_DIAMONDS, self::SUIT_DIAMONDS_SYMB);
    }

    public static function clubs(): Suit
    {
        return new Suit(self::SUIT_CLUBS, self::SUIT_CLUBS_SYMB);
    }

    public static function spades(): Suit
    {
        return new Suit(self::SUIT_SPADES, self::SUIT_SPADES_SYMB);
    }

    public function value(): string
    {
        return $this->suit;
    }

    public function symbol(): string
    {
        return $this->symbol;
    }

    public function color(): string
    {
        return $this->color;
    }

    public function equals(Suit $suit): bool
    {
        if ($this->suit === $suit->value()
            && $this->symbol === $suit->symbol()
            && $this->color === $suit->color()
        ) {
            return true;
        }

        return false;
    }
}
