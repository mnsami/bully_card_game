<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Card;

use BullyCardGame\Domain\Model\Card\Exception\SorryInvalidRank;

final class Rank
{
    /** @var string */
    private $rank;

    private const ACE = "ace";
    private const KING = "king";
    private const QUEEN = "queen";
    private const JACK = "jack";

    public const RANKS = [
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        self::JACK,
        self::QUEEN,
        self::KING,
        self::ACE
    ];

    public function __construct(string $rank)
    {
        if (!in_array($rank, self::RANKS)) {
            throw new SorryInvalidRank("Provided invalid card rank.");
        }
        $this->rank = $rank;
    }

    public function rank(): string
    {
        return $this->rank;
    }

    public function equals(Rank $rank): bool
    {
        if ($this->rank === $rank->rank()) {
            return true;
        }

        return false;
    }
}
