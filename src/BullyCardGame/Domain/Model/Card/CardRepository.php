<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Card;

use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Player\PlayerId;

interface CardRepository
{
    public function push(Card $card);

    public function pop(): ?Card;

    public function isEmpty(): bool;

    /**
     * @return Card[]
     */
    public function cards(): array;
}
