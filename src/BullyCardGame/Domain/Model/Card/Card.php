<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Card;

final class Card
{
    /** @var Suit */
    private $suit;

    /** @var Rank */
    private $rank;

    public function __construct(Suit $suit, Rank $rank)
    {
        $this->suit = $suit;
        $this->rank = $rank;
    }

    public function suit(): Suit
    {
        return $this->suit;
    }

    public function symbol(): string
    {
        return $this->suit->symbol();
    }

    public function rank(): Rank
    {
        return $this->rank;
    }

    public function toString(): string
    {
        return $this->symbol() . " {$this->rank->rank()}";
    }

    public function equals(Card $card): bool
    {
        if ($card->suit->equals($card->suit())
            && $card->rank->equals($this->rank)
        ) {
            return true;
        }

        return false;
    }
}
