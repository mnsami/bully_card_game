<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Player;

use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Player\Exception\SorryInvalidCardType;

final class Player
{
    /** @var PlayerId  */
    private $id;

    /** @var string  */
    private $name;

    /** @var Card[] */
    private $cards;

    public function __construct(PlayerId $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->cards = [];
    }

    public function id(): string
    {
        return $this->id->id();
    }

    public function playerId(): PlayerId
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function cards(): array
    {
        return $this->cards;
    }

    public function take(Card $card)
    {
        $this->cards[] = $card;
    }

    /**
     * @param Card[] $cards
     */
    public function deal(array $cards)
    {
        /** @var Card[] $card */
        foreach ($cards as $card) {
            if (! $card instanceof Card) {
                throw new SorryInvalidCardType("Invalid type. Only Card type accepted.");
            }

            $this->cards[] = $card;
        }
    }

    public function playCard(Card $card)
    {
        foreach ($this->cards as $idx => $playerCard) {
            if ($playerCard->equals($card)) {
                unset($this->cards[$idx]);
                break;
            }
        }
    }

    public function outOfCards(): bool
    {
        if (count($this->cards) == 0) {
            return true;
        }

        return false;
    }
}
