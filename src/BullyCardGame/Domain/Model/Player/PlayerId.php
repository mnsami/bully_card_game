<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Player;

use Ramsey\Uuid\Uuid;

final class PlayerId
{
    /** @var string */
    private $id;

    public function __construct(?string $id = null)
    {
        $this->id = null === $id ? Uuid::uuid4()->toString() : $id;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function equals(PlayerId $playerId)
    {
        return $this->id() === $playerId->id();
    }

    public function __toString(): string
    {
        return $this->id();
    }
}
