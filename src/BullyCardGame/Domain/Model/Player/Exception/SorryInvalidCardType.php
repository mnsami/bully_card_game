<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Player\Exception;

class SorryInvalidCardType extends \RuntimeException
{
}
