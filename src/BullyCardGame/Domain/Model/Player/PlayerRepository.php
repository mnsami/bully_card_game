<?php
declare(strict_types=1);

namespace BullyCardGame\Domain\Model\Player;

use BullyCardGame\Domain\Model\Card\Card;
use BullyCardGame\Domain\Model\Card\Rank;
use BullyCardGame\Domain\Model\Card\Suit;

interface PlayerRepository
{
    public function add(Player $player);

    public function ofId(PlayerId $playerId): ?Player;

    public function nextIdentity(): PlayerId;

    /**
     * @return Player[]
     */
    public function players(): array;

    public function playerCardWith(PlayerId $playerId, string $color, Rank $rank): ?Card;
}
